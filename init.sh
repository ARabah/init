#!/bin/bash

echo Clone répo
git clone https://gitlab.com/ARabah/compose.git docker
git clone https://gitlab.com/ARabah/films.git films
git clone https://gitlab.com/ARabah/oauth.git oauth
git clone https://gitlab.com/ARabah/reserve.git reserve


echo installation des image docker
docker image build -t ynov:symfony6 docker/web
docker image build -t ynov:postgres-15 docker/postgres

echo creation du network ynov
docker network create ynov


echo initialisation de la base de donné ainsi que de symfony \n
echo web
docker run --rm -tu 1000 -v ./films:/var/www/app ynov:symfony6 composer install --no-cache
docker run --rm -tu 1000 -v ./oauth:/var/www/app ynov:symfony6 composer install --no-cache
docker run --rm -tu 1000 -v ./reserve:/var/www/app ynov:symfony6 composer install --no-cache
echo bdd
BD_MOVIE=$(docker container create -v ./postgres/movie:/var/lib/postgresql/data -e POSTGRES_DB=ynov -e "POSTGRES_INITDB_ARGS=--locale-provider=icu --icu-locale=fr-FR" -e POSTGRES_PASSWORD=ynov -e "POSTGRES_USER=ynov" -e "TZ=Europe/Paris" ynov:postgres-15)
BD_OAUTH=$(docker container create -v ./postgres/oauth:/var/lib/postgresql/data -e POSTGRES_DB=ynov -e "POSTGRES_INITDB_ARGS=--locale-provider=icu --icu-locale=fr-FR" -e POSTGRES_PASSWORD=ynov -e "POSTGRES_USER=ynov" -e "TZ=Europe/Paris" ynov:postgres-15)
BD_RESERVE=$(docker container create -v ./postgres/reserve:/var/lib/postgresql/data -e POSTGRES_DB=ynov -e "POSTGRES_INITDB_ARGS=--locale-provider=icu --icu-locale=fr-FR" -e POSTGRES_PASSWORD=ynov -e "POSTGRES_USER=ynov" -e "TZ=Europe/Paris" ynov:postgres-15)
docker start $BD_MOVIE
docker start $BD_OAUTH
docker start $BD_RESERVE
docker exec $BD_MOVIE pg_isready
docker exec $BD_OAUTH pg_isready
docker exec $BD_RESERVE pg_isready
while [$? -ne 0]; do
    sleep 1
    docker exec $BD_MOVIE pg_isready
    docker exec $BD_OAUTH pg_isready
    docker exec $BD_RESERVE pg_isready
done
docker cp ./docker/Movies.sql $BD_MOVIE:/resource.sql
docker cp ./docker/Oauth.sql $BD_OAUTH:/resource.sql
docker cp ./docker/Reserve.sql $BD_RESERVE:/resource.sql
docker exec -itu postgres $BD_MOVIE psql -U ynov ynov -f resource.sql
docker exec -itu postgres $BD_OAUTH psql -U ynov ynov -f resource.sql
docker exec -itu postgres $BD_RESERVE psql -U ynov ynov -f resource.sql
docker exec $BD_MOVIE rm resource.sql
docker exec $BD_OAUTH rm resource.sql
docker exec $BD_RESERVE rm resource.sql
docker stop $BD_MOVIE
docker stop $BD_OAUTH
docker stop $BD_RESERVE
docker rm $BD_MOVIE
docker rm $BD_OAUTH
docker rm $BD_RESERVE

echo initialisation du composer
echo
cp docker/.env.default docker/.env
cp docker/default-composer.yml docker/composer.yml
read -e -p "Entrez le chemin absolu vers le dossier de la base de donner ou du volume (exemple '/home/user/projet/postgres' <- en linux) : " BDDPATH 
sed "s/DB_VOLUME=/DB_VOLUME=${BDDPATH}/" docker/.env
read -e -p "Entrez le chemin absolu vers le dossier du clone Git :  (exemple '/home/user/projet' <- en linux) : " GITPATH 
sed "s/WEB_VOLUME=/WEB_VOLUME=${GITPATH}/" docker/.env
