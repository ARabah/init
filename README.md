# Service Web de Gestion de Films

## Réalisé par

- ASSEUM Rabah
- RICOTTA Giovanni
- Valentin

## Installation avec Docker

### Pour les environement linux

0. Ouvré un terminale
1. Cloné le projet

```bash
git clone git@gitlab.com:ARabah/init.git WebService_Rabah_Valentin_Giovanni #si le conmpte gitlab a la clé publique ssh
git clone https://gitlab.com/ARabah/init.git WebService_Rabah_Valentin_Giovanni #si le compte gitlab n'a pas de clé publique ssh
```

Executé le fichier init.sh

```bash
./init.sh
```

### Pour les environement Windows

0. Ouvré un terminale
1. Cloné le projet

```bash
git clone git@gitlab.com:ARabah/init.git WebService_Rabah_Valentin_Giovanni #si le conmpte gitlab a la clé publique ssh
git clone https://gitlab.com/ARabah/init.git WebService_Rabah_Valentin_Giovanni #si le compte gitlab n'a pas de clé publique ssh
```

2. Allez dans dans le dossier **WebService_Rabah_Valentin_Giovanni** sauf si c'est nomé différament

```bash
cd WebService_Rabah_Valentin_Giovanni
```

3. Suive la commande suivante et ne surtout pas changé les noms des dossier

```bash
git clone https://gitlab.com/ARabah/compose.git docker
git clone https://gitlab.com/ARabah/films.git films
git clone https://gitlab.com/ARabah/oauth.git oauth
git clone https://gitlab.com/ARabah/reserve.git reserve
```

4. Accédez au dossier *docker*.
5. Copiez les fichiers **default-compose.yml** et **.env.default**, puis renommez-les en ***compose.yml*** et ***.env***.
6. Ouvrez le fichier **.env** et renseignez le port web souhaité ainsi que le chemin absolu vers le dossier du clone Git dans (exemple '/home/user/projet' <- en linux pour WEB_VOLUME et '/home/user/projet/postgres' pour la base de donnée) :
    - WEB_VOLUME
    - DB_VOLUME

7. Crée le network *ynov*

```bash
docker network create ynov
```

8. En restant dans le dossier *docker* construisé les images qui son utilisé dans le projet

```bash
docker image build -t ynov:symfony6 web
docker image build -t ynov:postgres-15 postgres
```

9. Retourné dans le dossier **WebService_Rabah_Valentin_Giovanni** sauf appelait autrement

```bash
cd ..
```

10. Initialisé les web service

```bash
docker run --rm -tu 1000 -v ./films:/var/www/app ynov:symfony6 composer install --no-cache
docker run --rm -tu 1000 -v ./oauth:/var/www/app ynov:symfony6 composer install --no-cache
docker run --rm -tu 1000 -v ./reserve:/var/www/app ynov:symfony6 composer install --no-cache
```

11. Initialisé la base de donné

```bash
docker container create -v ./postgres/movie:/var/lib/postgresql/data -e POSTGRES_DB=ynov -e "POSTGRES_INITDB_ARGS=--locale-provider=icu --icu-locale=fr-FR" -e POSTGRES_PASSWORD=ynov -e "POSTGRES_USER=ynov" -e "TZ=Europe/Paris" --name initBDDMovie ynov:postgres-15

docker container create -v ./postgres/oauth:/var/lib/postgresql/data -e POSTGRES_DB=ynov -e "POSTGRES_INITDB_ARGS=--locale-provider=icu --icu-locale=fr-FR" -e POSTGRES_PASSWORD=ynov -e "POSTGRES_USER=ynov" -e "TZ=Europe/Paris" --name initBDDOauth ynov:postgres-15

docker container create -v ./postgres/reserve:/var/lib/postgresql/data -e POSTGRES_DB=ynov -e "POSTGRES_INITDB_ARGS=--locale-provider=icu --icu-locale=fr-FR" -e POSTGRES_PASSWORD=ynov -e "POSTGRES_USER=ynov" -e "TZ=Europe/Paris" --name initBDDReserve ynov:postgres-15

docker start initBDDMovie
docker start initBDDOauth
docker start initBDDReserve

docker cp ./docker/Movies.sql initBDDMovie:/resource.sql
docker cp ./docker/Oauth.sql initBDDOauth:/resource.sql
docker cp ./docker/Reserve.sql initBDDReserve:/resource.sql

docker exec -itu postgres initBDDMovie psql -U ynov ynov -f resource.sql
docker exec -itu postgres initBDDOauth psql -U ynov ynov -f resource.sql
docker exec -itu postgres initBDDReserve psql -U ynov ynov -f resource.sql

docker exec initBDDMovie rm resource.sql
docker exec initBDDOauth rm resource.sql
docker exec initBDDReserve rm resource.sql

docker stop initBDDMovie
docker stop initBDDOauth
docker stop initBDDReserve

docker rm initBDDMovie
docker rm initBDDOauth
docker rm initBDDReserve
```

12. Et Enfin lancé le composer

```bash
cd docker
docker compose up -d #le -d n'est pas obligatoire mais vous pourais pas utilisé le terminalle en cours
```

## Directives d'utilisation

### GET /film/:id

1. Description

Permet de récupérer les informations d'un film spécifié par son identifiant (id).

2. Paramètres

> id (obligatoire) : L'identifiant unique du film.

3. Réponse

La réponse peut être obtenue au format JSON ou XML, selon l'en-tête "Content-Type" de la requête.

Format JSON:

```json
{
  "id": "ID du film",
  "nom": "Nom du Film",
  "description": "Description du film",
  "date": "Date du film",
  "note": "Note du Film"
}
```

Format HAL+JSON:

```json
{
  "links": {
    "href": "/route",
    "rel": "entité",
    "type": "méthode"
  },
  "id": "ID du film",
  "nom": "Nom du Film",
  "description": "Description du film",
  "date": "Date du film",
  "note": "Note du Film"
}
```

Format XML:

```xml
<?xml version="1.0"?>
<films>
  <id>: ID du film <id>
  <nom>: Nom du Film <nom>
  <description>: Description du film <description>
  <date>: Date du film <date>
  <note>: Note du Film <note>
</films>
```

Format HAL+XML:

```xml
<?xml version="1.0"?>
<films>
  <_links>
    <href>/route</href>
    <rel>entité</rel>
    <type>méthode</type>
  </_links>
  <id>: ID du film <id>
  <nom>: Nom du Film <nom>
  <description>: Description du film <description>
  <date>: Date du film <date>
  <note>: Note du Film <note>
</films>
```

4. Retours Possibles

404 Not Found
    Ressource non trouvée.

Statut HTTP 200 :
> OK

Statut HTTP 404 :
> Not Found

---

### DELETE /film/:id

1. Description

Cet endpoint permet de supprimer les informations d'un film spécifié par son identifiant (id).

2. Paramètres

> id (obligatoire) : L'identifiant unique du film à supprimer.

3. Réponse

404 Not Found
    Ressource non trouvée.

Statut HTTP 200
> OK

Statut HTTP 404
> Not Found

---

### GET /films

1. Description

Permet de récupérer la liste complète des films disponibles.

2. Réponse

La réponse peut être obtenue au format JSON ou XML, selon l'en-tête "Accept" de la requête.

Format JSON:

```json
[
 {
   "id": "ID du film 1",
   "nom": "Nom du film 1",
   "description": "Description du film 1",
   "date": "Date du film",
   "note": "Note du film 1"
 }
 {
   "id": "ID du film 2",
   "nom": "Nom du film 2",
   "description": "Description du film 2",
   "date": "Date du film",
   "note": "Note du film 2"
 }
]
```

Format XML:

```xml
<0>
 <id>: ID du film 1 <id>
   <nom>: Nom du film 1 <nom>
   <description>: Description du film 1 <description>
   <date>: Date du film <date>
   <note>: Note du film 1 <note>
</0>
<1>
 <id>: ID du film 2 <id>
   <nom>: Nom du film 2 <nom>
   <description>: Description du film 2 <description>
   <date>: Date du film <date>
   <note>: Note du film 2 <note>
</1>
```

3. Retours Possibles

404 Not Found
    Ressource non trouvée.

Statut HTTP 200
> OK

Statut HTTP 404
> Not Found

---

###  GET /format

1. Description

Cet endpoint permet de récupérer le format JSON des informations sur les films.

### POST /film/creation/:nom/:description/:date/:note

1. Description

Permet de créer un nouvel objet.

2. Paramètres

> nom (obligatoire) : nom du film.

> description (obligatoire) : description du film.

> date (obligatoire) : date de parution du film.

> note (obligatoire) : note attribuée au film.

> id (obligatoire) : L'identifiant unique du film à supprimer.

3. Réponse

404 Not Found
    Ressource non trouvée.

Statut HTTP 201
> Création réussie

Statut HTTP 404
> Not Found

---

### PUT/PATCH /film/:id/modif/:nom/:description/:date/:note

1. Description

Permet de modifier un objet.

2. Paramètres

> id (obligatoire) : L'identifiant unique du film.

> nom (pas obligatoire) : nom du film, mettre la valeur **null** si aucune modification n'est souhaitée avec la méthode **PATCH**.

> description (pas obligatoire) : description du film, mettre la valeur **null** si aucune modification n'est souhaitée avec la méthode **PATCH**.

> date (pas obligatoire) : date de parution du film, mettre la valeur **null** si aucune modification n'est souhaitée avec la méthode **PATCH**.

> note (pas obligatoire) : note attribuée au film, mettre la valeur **null** si aucune modification n'est souhaitée avec la méthode **PATCH**.

3. Réponse

Statut HTTP 200
> Ressource modifiée

Statut HTTP 422
> Ressource introuvable

---

### GET /film/recherche/:typeRecherche/:value

1. Description

Permet de rechercher un film par son titre ou sa description.

2. Paramètres

> typeRecherche (obligatoire) : doit être soit **titre** ou **description**.

> value (obligatoire) : texte à rechercher.

3. Réponse

Statut HTTP 200
> Ressource trouvée

Statut HTTP 422
> Ressource introuvable
